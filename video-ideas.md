# General ideas

## Series ideas

* Draw my life style cartoons
* Superviro remixes
* Make a sitcom with The Sims
* "Kabe Diras" show where Kabe (the cat) is a TV host

## Movie ideas

Try to make solo short movies?

Make movies by animating?

Make movies with machinimation?

## Other dang ideas

* Volunteering for political campaigns
* Places in Kansas City
* Ranting about the software industry
* Making zines
* Making a visual novel in esperanto
* Making C++ in Esperanto
* Childrens' style video storybooks
* Parodies of tv / video games / movies
* Translate pop songs and play/sing them
* Macro video... use tiny figures to make movies around the house

# Serieses

## Draw My Life

* Fears I had as a child (curly slides, flash floods, burgulars)
* Pet peeves
* Genderfluid thoughts
* The year Rai was gone :(
* Rai and my first date
* Favorite types of movies (Lost Skeleton of Cadavra, The Room)
* Childhood movie making
* My gamedev history
* "So... why do you want this job?" - Job interviews

## Rejcx faras ion

* Rachel draws a comic
* Rachel mods a video game
* Rachel programs a text adventure
* Rachel plays piano
* Rachel crochets a doll
* Rachel goes to Half Price Books
* Rachel reads a book
* Rachel creates a zine

## Let's play

* Overwatch?
* Paladins?
* SuperTux
* TuxKart
* Dink Smallwood
* Freedroid RPG

 
